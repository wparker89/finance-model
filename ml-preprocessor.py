import pandas as pd
from collections import Counter
import numpy as np
from sklearn import svm, cross_validation, neighbors
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from statistics import mean
import pickle

# Starting strategy (simple)
# To start, let's say a company is a buy if, within the next 7 days, 
# its price goes up more than 2% and it is a sell if the price goes 
# down more than 2% within those 7 days.

# Each model currently trained on a single company.
def process_data_for_data_labels(ticker): # Handle when ticker entered doesn't exist
	how_many_days = 7 
	df = pd.read_csv('sp500_joined_closes.csv', index_col=0)
	tickers = df.columns.values.tolist() # tolist() might be unncessary.  check docs.
	df.fillna(0, inplace=True) # Fill any missing data with 0.  Might want to change this strategy later.

	# Now get % change values over the seven day range
	# Create new datafram columns 
	for i in range(1, how_many_days + 1):
		df['{}_{}d'.format(ticker, i)] = (df[ticker].shift(-i) - df[ticker]) / df[ticker]

	df.fillna(0, inplace=True)

	return tickers, df

# Function will be mapped to pandas dataframe column telling us what to do.
# Basic for now, can be improved.  Doesn't cover all scenarios.
def buy_sell_hold(*args):
	cols = [c for c in args]
	requirement = 0.02 # 2% change

	buy_sell_hold = 0 
	for col in cols:
		if col > requirement:
			buy_sell_hold = 1 # Buy
		elif col < -requirement:
			buy_sell_hold = -1 # Sell
		else:
			buy_sell_hold = 0 # Hold
	return buy_sell_hold

# Takes any ticker, creates needed dataset and then creates target column i.e our label.
def extract_featuresets(ticker):
	tickers, df = process_data_for_data_labels(ticker)

	df['{}_target'.format(ticker)] = list(map(buy_sell_hold,
		df['{}_1d'.format(ticker)],
		df['{}_2d'.format(ticker)],
		df['{}_3d'.format(ticker)],
		df['{}_4d'.format(ticker)],
		df['{}_5d'.format(ticker)],
		df['{}_6d'.format(ticker)],
		df['{}_7d'.format(ticker)]))

	vals = df['{}_target'.format(ticker)].values.tolist()
	str_vals = [str(i) for i in vals]
	#print('Data spread: ', Counter(str_vals))

	# Clean up the data
	df.fillna(0, inplace=True)
	df = df.replace([np.inf, -np.inf], np.nan)
	df.dropna(inplace=True)

	# The idea here being that some companies will change in price before others, 
	# and we can profit maybe on the laggards. We'll convert the stock prices to % changes:
	df_vals = df[[ticker for ticker in tickers]].pct_change()
	df_vals = df_vals.replace([np.inf, -np.inf], 0)
	df_vals.fillna(0, inplace=True)

	X = df_vals.values # featuresets
	y = df['{}_target'.format(ticker)].values # targets / labels

	return X, y, df

def do_ml(ticker):
	X, y, df = extract_featuresets(ticker)
	# Shuffle data and create training and testing samples
	X_train, x_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.25)

	# Just one classifier
	# clf = neighbors.KNeighborsClassifier()
	# Try a voting classifier
	clf = VotingClassifier([('lsvc', svm.LinearSVC()), 
		('knn', neighbors.KNeighborsClassifier()), 
		('rfor', RandomForestClassifier())])


	clf.fit(X_train, y_train)
	confidence = clf.score(x_test, y_test)

	# Output
	#print('Accuracy:', confidence)
	predictions = clf.predict(x_test)
	#print('Predicted class counts: ', Counter(predictions))
	#print()
	#print()

	return confidence # Don't like interchange between terms accuracy and confidence

with open('sp500tickers.pickle', 'rb') as file:
	tickers = pickle.load(file)

accuracies = []
for count, ticker in enumerate(tickers):
	if count % 10 == 0:
		print(count)

	accuracy = do_ml(ticker)
	accuracies.append(accuracy)
	print('{} accuracy: {}.'.format(ticker, accuracy))
print('Average accuracy: {}'.format(mean(accuracies)))