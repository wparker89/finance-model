import bs4 as bs
import pickle
import requests
import datetime as dt
import os 
import pandas as pd
import pandas_datareader.data as web
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np

style.use('ggplot') # Make the plots look nicer than the standard.

# Nasty global variables for the minute.
# refactor into class.  Has some variables needed in more than one function
# Some methods requires data to exist i.e other methods to have been run first.
dir_name = 'stock_dfs' 

# Encountered a problem where wikipedia and Yahoo have some discrepancies with company names.  
# See here: https://github.com/pydata/pandas-datareader/issues/170
# Solution for now is to modify the data specifically the '.' in company names scraped from wikipedia
# and replace with a '-' as they are named in Yahoo.  Would like a better solution.

def save_sp500_tickers():
	resp = requests.get('http://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
	soup = bs.BeautifulSoup(resp.text, 'lxml')
	table = soup.find('table', {'class' : 'wikitable sortable'})

	tickers = []
	for row in table.findAll('tr')[1:]:
		ticker = row.findAll('td')[0].text

		if '.' in ticker:
			ticker = ticker.replace('.', '-') # Replace returns a new copy of a string.

		tickers.append(ticker)

	with open('sp500tickers.pickle', 'wb') as f:
		pickle.dump(tickers, f)

	return tickers

def get_data_from_yahoo(reload_sp500=False):
	if reload_sp500:
		tickers = save_sp500_tickers()
	else:
		with open('sp500tickers.pickle', 'rb') as file:
			tickers = pickle.load(file)

	if not os.path.exists(dir_name):
		os.makedirs(dir_name)

	# Retrieve the data
	start = dt.datetime(2000, 1, 1)
	end = dt.datetime.today()

	for ticker in tickers:
		ticker_file = '{}/{}.csv'.format(dir_name, ticker)
		# Just experimenting at the moment so let's not worry about working on the most recent data.
		if not os.path.exists(ticker_file) or (dt.datetime.fromtimestamp(os.path.getmtime(ticker_file)) - dt.datetime.today()) > dt.timedelta(days = 5):
			df = web.DataReader(ticker, 'yahoo', start, end) # Can use other things like google as well?
			df.to_csv(ticker_file)
		else:
			print('Already have {}'.format(ticker))

def compile_data():
	with open('sp500tickers.pickle', 'rb') as file:
		tickers = pickle.load(file)

	main_df = pd.DataFrame()

	for count, ticker in enumerate(tickers):
		ticker_file = '{}/{}.csv'.format(dir_name, ticker)
		df = pd.read_csv(ticker_file)
		df.set_index('Date', inplace=True)
		
		# Possible analysis to consider for the future.
		# df['{}_HL_pct_diff'.format(ticker)] = (df['High'] - df['Low']) / df['Low']
		# df['{}_daily_pct_chng'.format(ticker)] = (df['Close'] - df['Open']) / df['Open']
		df.rename(columns={'Adj Close' : ticker}, inplace=True)
		df.drop(['Open', 'High', 'Low', 'Close', 'Volume'], 1, inplace=True)
		
		if main_df.empty:
			main_df = df
		else:
			main_df = main_df.join(df, how='outer')

		if count % 10 == 0:
			print(count)

	print(main_df.head())
	main_df.to_csv('sp500_joined_closes.csv')


# Proabably want to analyse over a shorter time period?
# If you want a diversified portfolio, you want to invest in non-correlated stocks

def visualise_data(savefig=False):
	df = pd.read_csv('sp500_joined_closes.csv') # Naming inconsistencies

	# Construct a correlation table
	df_corr = df.corr()
	print(df_corr.head())
	df_corr.to_csv('sp500corr.csv')

	# Construct a heatmap to analyse the correlations
	data1 = df_corr.values

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(1, 1, 1)

	# This gives red for negative correlations, yellow for no correlation, green for positive correlations
	heatmap1 = ax1.pcolor(data1, cmap=plt.cm.RdYlGn)
	fig1.colorbar(heatmap1)

	# Is data shape the correct way around?
	ax1.set_xticks(np.arange(data1.shape[1]) + 0.5, minor=False)
	ax1.set_yticks(np.arange(data1.shape[0]) + 0.5, minor=False)

	ax1.invert_yaxis()
	ax1.xaxis.tick_top()

	column_labels = df_corr.columns
	row_labels = df_corr.index
	ax1.set_xticklabels(column_labels)
	ax1.set_yticklabels(row_labels)

	plt.xticks(rotation=90) # Rotates the ticks so that they are vertical, to improve readability.
	heatmap1.set_clim(-1, 1) # Colour range goes from [-1, 1] 
	plt.tight_layout() # Helps with plot formatting

	if savefig:
		plt.savefig('correlations.png', dpi=(300))

	plt.show()